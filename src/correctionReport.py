import copy
from datetime import datetime, timezone

import pandas
import pandas as pd
from pymongo import MongoClient


def eligibility_correction_report():

    global count
    client = MongoClient(
        'mongodb://connectUser:connect%40vas@isn-con-mg-p1.dstvo.local:27017/?authSource=ddm-connect-vas&readPreference=nearest&appname=MongoDB%20Compass&ssl=false')
    filter_data = {
        'lastModifiedDate': {
            '$gte': datetime(2021, 7, 4, 8, 8, 49, tzinfo=timezone.utc)
        }
    }
    project = {
        '_id': 1,
        'country': 1,
        'customerNumber': 1,
        'agreementId': 1,
        'status': 1,
        'cause': 1
    }
    result = client['ddm-connect-vas']['eligibility-correction-report'].find(
        filter=filter_data,
        projection=project
    )
    today = datetime.today()
    today = today.strftime("%m-%d-%Y")
    # read_csv = pd.read_csv('panda.csv')
    output = copy.deepcopy(result)
    export_as_excel(output, today)
    # Convert the mongo docs to a DataFrame
    docs = pandas.DataFrame(result)
    # export MongoDB documents to a CSV file, leaving out the row "labels" (row numbers)
    docs.to_csv('eligibility-correction-report-' + today + '.csv', ",", index=False)  # CSV delimited by commas


def export_as_excel(output, today):
    global count
    data_frame = pd.DataFrame(list(output), columns=['country', 'status'])
    group_by_series = data_frame.groupby(['country', 'status'])['status'].count()
    frame = pd.DataFrame(group_by_series)
    status_ = frame['status']
    report = {}
    for data, row in status_.items():
        country = str(data[0])
        status = str(data[1])
        count = row
        country_data = report.get(country)
        if country_data is not None:
            country_data[status] = count
            status_details = country_data
        else:
            status_details = {status: count}
            report[country] = status_details
    pd_data_frame = pd.DataFrame(data=report)
    pd_data_frame.to_excel('eligibility-correction-report-' + today + '.xlsx')
    print(pd_data_frame.head(5))


eligibility_correction_report()
